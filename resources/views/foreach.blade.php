@extends ('master')
@section('konten')
<main role="main" class="container">
    <br>
    <font>
        <h2 align="center">Foreach</h2>
    </font>
    <hr>
    <div>
        <?php
        $array = array("0", "1", " 2", "3", "4", "5");
        ?>
        @foreach ($array as $value)
        {{ $value }}
        @endforeach
    </div>
    <hr>
</main>
@endsection